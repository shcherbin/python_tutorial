install:
	( \
		virtualenv env; \
		source env/bin/activate; \
		pip install -r requirements.txt; \
	)
start:
	( \
		source env/bin/activate; \
		jupyter notebook; \
	)
