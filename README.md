Contents
========

1. Basic Data Types
2. Flow Control
3. Exception Handling
4. Built Ins
5. List|Dict|Set Comprehentions
6. Generator Functions|Expressions
7. Modules and Packages
8. Functions and Lambdas
8. Context Managers
9. Object Oriented Programming
10. Decorators
11. Descriptors

Run locally
===========
1. Run `make`
2. Activate virtual env
`source env/bin/activate`
3. `cd` to the `lessons` folder and type: `jupyter notebook`